import json
import collections
import urllib
from scylla import configuration
from scylla import RestClient


configuration.setDefaults('Magento2', {
    #'url': None,
    #'token': None,
    'debug': False,
    'verify_ssl': True,
})


class MagentoRestError(RestClient.RestError):
    pass


class MagentoRestClient(RestClient):
    RestError = MagentoRestError
    __instance = None

    def __init__(self):
        config = configuration.getSection('Magento2')

        url = config['url'] + '/rest'

        super(MagentoRestClient, self).__init__(url,
            verify_ssl=config['verify_ssl'],
            debug=config['debug'])

        self._set_header('Authorization', 'Bearer {}'.format(config['token']))
        self._set_header('Content-Type', 'application/json')

        self.name = config['name']

    @classmethod
    def get_instance(cls):
        if not cls.__instance:
            cls.__instance = cls()
        return cls.__instance

    def get(self, path, params=None):
        if params and isinstance(params, collections.MutableMapping):
            params = self.__flatten(params)
        return super(MagentoRestClient, self).get(path, params)

    def __flatten(self, params, parent_key=''):
        '''Flattens a nested dictionary into keys like "a[0][b]".'''
        items = []
        for k, v in params.items():
            new_key = '{}[{}]'.format(parent_key, k) if parent_key else k
            if isinstance(v, list):
                v = dict(enumerate(v))
            if isinstance(v, collections.MutableMapping):
                items.extend(self.__flatten(v, new_key).items())
            else:
                items.append((new_key, v))
        return dict(items)
