from scylla import records


class MagentoRecord(records.ParsedRecord):

    key_field = 'entity_id'

    factory_base_classname = 'Magento'
    classname = 'MagentoRecord'

    _datetime_fields = ['created_at', 'updated_at']
    _int_fields = ['id', 'entity_id', 'parent_id']
    _decimal_fields = []

    # links to related records to _{field} by using {field}_id
    _linked_by_id = {}

    def _process_fields(self):
        # automatically link up _id fields to records
        for (field, classname) in self._linked_by_id.items():
            id_field = field + '_id'
            obj_field = '_' + field

            # make sure it's in the linked records
            self._linked_records[obj_field] = classname

            if self.get(id_field) and not self.get(obj_field):
                # add placeholder obj with just the id
                cls = MagentoRecord.get_subclass(classname)
                self.data[obj_field] = { cls.key_field: self.get(id_field) }

        super(MagentoRecord, self)._process_fields()

        # set key/value dict for custom attribute list
        self.data['_attributes'] = {}
        for attr in self.get('custom_attributes', []):
            self.data['_attributes'][attr['attribute_code']] = attr['value']


class MagentoAddress(MagentoRecord):

    key_field = 'id'

    _linked_records = {
        'region': 'Region',
    }

    _int_fields = ['id', 'customer_id', 'region_id', 'website_id']


class MagentoCreditMemo(MagentoRecord):

    _linked_by_id = {
        'invoice': 'Invoice',
        'order': 'Order',
        'shipping_address': 'OrderAddress',
        'billing_address': 'OrderAddress',
    }

    _int_fields = [
        'entity_id',
        'invoice_id',
        'order_id',
        'store_id',
        'shipping_address_id',
        'billing_address_id',
    ]
    _decimal_fields = [
        'adjustment',
        'adjustment_negative',
        'adjustment_positive',
        'base_adjustment',
        'base_adjustment_negative',
        'base_adjustment_positive',
        'base_discount_amount',
        'base_grand_total',
        'base_discount_tax_compensation_amount',
        'base_shipping_amount',
        'base_shipping_discount_tax_compensation_amnt',
        'base_shipping_incl_tax',
        'base_shipping_tax_amount',
        'base_subtotal',
        'base_subtotal_incl_tax',
        'base_tax_amount',
        'discount_amount',
        'grand_total',
        'discount_tax_compensation_amount',
        'shipping_amount',
        'shipping_discount_tax_compensation_amount',
        'shipping_incl_tax',
        'shipping_tax_amount',
        'subtotal',
        'subtotal_incl_tax',
        'tax_amount',
    ]


class MagentoCustomer(MagentoRecord):

    key_field = 'id'

    _sub_records = {
        'addresses': 'Address',
    }

    _int_fields = ['id', 'group_id', 'store_id', 'website_id',
        'default_billing', 'default_shipping']

    @property
    def name(self):
        return u'{} {}'.format(self.get('firstname'), self.get('lastname'))


class MagentoInvoice(MagentoRecord):
    '''An invoice is a record of the receipt of payment for an order.'''

    _int_fields = [
        'billing_address_id',
        'entity_id',
        'order_id',
        'shipping_address_id',
        'store_id',
        'total_qty',
    ]
    _decimal_fields = [
        'base_discount_amount',
        'base_grand_total',
        'base_discount_tax_compensation_amount',
        'base_shipping_amount',
        'base_shipping_discount_tax_compensation_amnt',
        'base_shipping_incl_tax',
        'base_shipping_tax_amount',
        'base_subtotal',
        'base_subtotal_incl_tax',
        'base_tax_amount',
        'base_total_refunded',
        'discount_amount',
        'grand_total',
        'discount_tax_compensation_amount',
        'shipping_amount',
        'shipping_discount_tax_compensation_amount',
        'shipping_incl_tax',
        'shipping_tax_amount',
        'subtotal',
        'subtotal_incl_tax',
        'tax_amount',
    ]

    _linked_by_id = {
        'order': 'Order',
        'billing_address': 'OrderAddress',
        'shipping_address': 'OrderAddress',
    }

    def _process_fields(self):
        super(MagentoInvoice, self)._process_fields()


class MagentoOrder(MagentoRecord):
    '''A sales order with items, address, and payment information'''

    _sub_records = {
        'payment': 'Payment',
        'billing_address': 'OrderAddress',
        '_shipping_addresses': 'OrderAddress',
    }
    _linked_by_id = {
        'customer': 'Customer',
    }

    _int_fields = [
      'billing_address_id',
      'customer_group_id',
      'customer_id',
      'entity_id',
      'quote_address_id',
      'quote_id',
      'store_id',
    ]
    _decimal_fields = [
      'adjustment_negative',
      'adjustment_positive',
      'base_adjustment_negative',
      'base_adjustment_positive',
      'base_discount_amount',
      'base_discount_canceled',
      'base_discount_invoiced',
      'base_discount_refunded',
      'base_discount_tax_compensation_amount',
      'base_discount_tax_compensation_invoiced',
      'base_discount_tax_compensation_refunded',
      'base_grand_total',
      'base_shipping_amount',
      'base_shipping_canceled',
      'base_shipping_discount_amount',
      'base_shipping_discount_tax_compensation_amnt',
      'base_shipping_incl_tax',
      'base_shipping_invoiced',
      'base_shipping_refunded',
      'base_shipping_tax_amount',
      'base_shipping_tax_refunded',
      'base_subtotal',
      'base_subtotal_canceled',
      'base_subtotal_incl_tax',
      'base_subtotal_invoiced',
      'base_subtotal_refunded',
      'base_tax_amount',
      'base_tax_canceled',
      'base_tax_invoiced',
      'base_tax_refunded',
      'base_total_canceled',
      'base_total_due',
      'base_total_invoiced',
      'base_total_invoiced_cost',
      'base_total_offline_refunded',
      'base_total_online_refunded',
      'base_total_paid',
      'base_total_qty_ordered',
      'base_total_refunded',
      'discount_amount',
      'discount_canceled',
      'discount_invoiced',
      'discount_refunded',
      'discount_tax_compensation_amount',
      'discount_tax_compensation_invoiced',
      'discount_tax_compensation_refunded',
      'grand_total',
      'payment_authorization_amount',
      'shipping_amount',
      'shipping_canceled',
      'shipping_discount_amount',
      'shipping_discount_tax_compensation_amount',
      'shipping_incl_tax',
      'shipping_invoiced',
      'shipping_refunded',
      'shipping_tax_amount',
      'shipping_tax_refunded',
      'subtotal',
      'subtotal_canceled',
      'subtotal_incl_tax',
      'subtotal_invoiced',
      'subtotal_refunded',
      'tax_amount',
      'tax_canceled',
      'tax_invoiced',
      'tax_refunded',
      'total_canceled',
      'total_due',
      'total_invoiced',
      'total_item_count',
      'total_offline_refunded',
      'total_online_refunded',
      'total_paid',
      'total_qty_ordered',
      'total_refunded',
      #'weight',
    ]

    def _process_fields(self):
        if not self.get('_shipping_addresses'):
            # process nested shipping addresses
            try:
                addresses = []
                for ship_assign in self['extension_attributes']['shipping_assignments']:
                    addresses.append(ship_assign['shipping']['address'])
                self.data['_shipping_addresses'] = addresses
            except KeyError:
                pass

        super(MagentoOrder, self)._process_fields()

    @property
    def shipping_address(self):
        try:
            return self['_shipping_addresses'][0]
        except (KeyError, IndexError):
            return self.get('billing_address')


class MagentoOrderAddress(MagentoRecord):

    key_field = 'entity_id'

    _int_fields = [
        'entity_id',
        'customer_address_id',
        'parent_id',
        'region_id',
    ]


class MagentoPayment(MagentoRecord):
    '''Payment information for an order'''

    _int_fields = [
        'entity_id',
        'parent_id',
        'quote_payment_id',
    ]
    _decimal_fields = [
        'amount_authorized',
        'amount_canceled',
        'amount_ordered',
        'amount_paid',
        'amount_refunded',
        'base_amount_authorized',
        'base_amount_canceled',
        'base_amount_ordered',
        'base_amount_paid',
        'base_amount_paid_online',
        'base_amount_refunded',
        'base_amount_refunded_online',
        'base_shipping_amount',
        'base_shipping_captured',
        'base_shipping_refunded',
        'shipping_amount',
        'shipping_captured',
        'shipping_refunded',
    ]


class MagentoProduct(MagentoRecord):

    # even though there is an integer id, the api endpoints key off of sku
    key_field = 'sku'

    _int_fields = ['id', 'attribute_set_id', 'status', 'visibility']
    _decimal_fields = ['price']

    _sub_records = {
        'options': 'ProductOption',
    }


class MagentoProductOption(MagentoRecord):

    key_field = 'option_id'

    _int_fields = ['option_id', 'sort_order']

    _sub_records = {
        'values': 'ProductOptionValue',
    }


class MagentoProductOptionValue(MagentoRecord):

    key_field = 'option_type_id'

    _int_fields = ['option_type_id', 'sort_order']
    _decimal_fields = ['price']


class MagentoRegion(MagentoRecord):

    key_field = 'region_id'

    _int_fields = ['region_id']


factory = MagentoRecord.factory
