"""Core connection to Magento 2 and an App to download updates.

configuration example:
    'Magento2': {
        'name': 'MyStore',
        'url': 'https://magento-example.com',
        'token': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        'debug': False,
        'verify_ssl': True,
        'version': 1,
        'tasks': [
            ('customers', 'Customer', '/customers/search'),
            ('orders', 'Order'),
            ('invoices', 'Invoice'),
            ('creditmemo', 'CreditMemo', '/creditmemos'),
        ],
        'on_demand_tasks': [
        ],
    },
"""

from .app import App

from .client import MagentoRestClient, MagentoRestError

from .tasks import MagentoTask

from . import records
