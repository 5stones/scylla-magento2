from scylla import app
from scylla import configuration
from .client import MagentoRestClient
from .tasks import MagentoTask
from .tasks import MagentoDetailedTask
from .tasks import MagentoOrderTask


configuration.setDefaults('Magento2', {
    'tasks': [
        ('customers', 'Customer', 'customers', 'customers/search'),
        ('orders', 'Order'),
        ('invoices', 'Invoice'),
        ('creditmemos', 'CreditMemo', 'creditmemo', 'creditmemos'),
        ('products', 'Product'),
    ],
    'on_demand_tasks': [
    ],
})


class App(app.App):

    main_options = [
        ('after', 'a', None),
        ('limit', 'l', 100),
        ('page', 'p', 0),
        ('retry-errors', 'r'),
    ]

    def _prepare(self):
        config = configuration.getSection('Magento2')

        self.client = MagentoRestClient()

        for task_params in config['tasks']:
            task = self.__create_task(task_params)
            self.tasks[task_params[0]] = task

        for task_params in config['on_demand_tasks']:
            task = self.__create_task(task_params)
            self.on_demand_tasks[task_params[0]] = task

    def __create_task(self, task_params):
        '''instanciates the correct task for the record type'''
        if task_params[0] == 'products':
            return MagentoDetailedTask(self.client, *task_params)
        elif task_params[0] == 'orders':
            return MagentoOrderTask(self.client, *task_params)
        else:
            return MagentoTask(self.client, *task_params)

    def get_task(self, record_type):
        try:
            return super(App, self).get_task(record_type)
        except KeyError:
            return MagentoTask(self.client, record_type)
