import re
from scylla import configuration
from scylla import tasks
from scylla import log
from scylla import orientdb
from .records import MagentoRecord
from pprint import pprint


_config = configuration.getSection('Magento2', {
    'version': 1,
    'site': 'default',
})


class MagentoTask(tasks.Task):
    '''Downloads records from Magento.'''

    def __init__(self, client, name, record_type=None, endpoint=None, search_path=None):
        # a singularize could be better, but this is fine
        self.record_type = record_type or re.sub('e?s$', '', name).title()

        self.client = client

        self.classname = '{0}{1}'.format(client.name, self.record_type)

        self.endpoint = self.with_base_url(endpoint or name)
        self.search_path = self.with_base_url(search_path or self.endpoint)

        task_id = '{0}'.format(self.classname)
        super(MagentoTask, self).__init__(task_id)

    @staticmethod
    def with_base_url(path):
        if path[0] != '/':
            return '/{}/V{}/{}'.format(_config['site'], _config['version'], path)
        return path

    def _step(self, after, options):
        filters = [{
            'field': 'updated_at',
            'condition_type': 'gt',
            'value': after or '0000-00-00T00:00:00Z',
        }]

        args = {
            'searchCriteria': {
                'filterGroups': [ { 'filters': filters } ],
                'currentPage': options.get('page', 0),
                'pageSize': options.get('limit', 100),
            },
        }

        print('\nGetting {} records updated after {} from {} :'.format(self.record_type, after, self.endpoint))
        self.get_all(args)

    def get_all(self, args={}):
        if 'currentPage' not in args['searchCriteria'] or not args['searchCriteria']['currentPage']:
            args['searchCriteria']['currentPage'] = 0

        response = None
        while response is None or len(response['items']) == args['searchCriteria']['pageSize']:
            response = self.client.get(self.search_path, args)
            self._process_search_response(response)
            args['searchCriteria']['currentPage'] += 1

    def _process_search_response(self, response):
        for obj in response['items']:
            self._process_response_record(obj)

    def _process_response_record(self, obj):
        rec = MagentoRecord.factory(self.client, self.record_type, obj)
        with log.ErrorLogging(self.task_id, rec):
            rec.throw_at_orient()
            self._link_children(rec)
            print(u'Saved {} {} as {} {}'.format(
                self.record_type, rec.get(rec.key_field), rec.classname, rec.rid))

    def _link_children(self, rec):
        """Once the record and subrecords are in orientdb, add _parent to children
            and children of children recursively, depth first.
        """
        # pull rids of children
        rids = []
        for subtype in rec._sub_records:
            value = rec.get(subtype)
            if isinstance(value, MagentoRecord):
                # single record
                rids.append(value.rid)
                # recursively link children of child
                self._link_children(value)
            elif value:
                # list of records
                for subrecord in value:
                    rids.append(subrecord.rid)
                    # recursively link children of child
                    self._link_children(subrecord)
        # run the update in orientdb
        q = "UPDATE [{}] SET _parent = {}".format(','.join(rids), rec.rid)
        orientdb.execute(q)

    def process_ids(self, ids):
        for rec_id in ids:
            self.process_one(rec_id)

    def process_one(self, rec_id):
        if (type(rec_id) == str):
            rec_id = unicode(rec_id, errors='ignore')
        obj = self.client.get(u'{}/{}'.format(self.endpoint, rec_id))
        self._process_response_record(obj)


class MagentoDetailedTask(MagentoTask):
    '''Downloads records with individual requests to get full details.'''

    def _process_search_response(self, response):
        '''Pull ids from search response and request records individually'''
        key_field = MagentoRecord.get_subclass(self.record_type).key_field
        ids = [obj[key_field] for obj in response['items']]
        self.process_ids(ids)


class MagentoOrderTask(MagentoTask):
    '''Downloads Order and updates the customer's _last_order_date.'''

    def _link_children(self, rec):
        super(MagentoOrderTask, self)._link_children(rec)

        # update the customer's order date so we can filter syncs by it
        try:
            customer_id = rec['customer_id']
            order_date = str(rec['created_at']).replace(' ', 'T')[:19] + 'Z'

            q = ("UPDATE {}Customer "
                "SET _last_order_date = '{}', _updated_at = date() "
                "WHERE id = '{}'")
            q = q.format(self.client.name, order_date, customer_id)
            orientdb.execute(q)
        except KeyError:
            pass
