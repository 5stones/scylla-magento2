from distutils.core import setup

version = '0.0.1'

setup(
    name = 'scylla-magento2',
    packages = ['scylla_magento2'],
    version = version,

    description = 'Scylla-Magento2 connects Scylla with a Magento 2 server for further integration.',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-magento2',
    download_url = 'https://gitlab.com/5stones/scylla-magento2/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'scylla',
    ],
    dependency_links=[
        'git+https://gitlab.com:5stones/scylla.git',
    ],
)
